import os
import anthropic
from pathlib import Path
from dotenv import load_dotenv

if Path(".env").exists():
    load_dotenv()

if "ANTHROPIC_API_KEY" not in os.environ:
    raise ValueError("Expected the environment variable ANTHROPIC_API_KEY to exist.")

# Loads API key from env var ANTHROPIC_API_KEY
client = anthropic.Anthropic()
model = "claude-3-haiku-20240307"
max_tokens = 50
pre_prompt = (
    "Write a fun or witty one-line fortune cookie message. "
    "Limit output to the message itself. "
    "Do not request additional info or text.\n"
)
input_pre_prompt = "Start the message with the following text:\n\n"

print(
    "\nWelcome to Fortune.Cookie.ai!\n"
    "==============================\n"
    "I will generate random fortune cookie wisdom.\n"
    "You can enter a short prefix if you wish to get less random wisdom.\n"
    "Enter 'q' to quit.\n"
)
while True:
    input_text = input("> ").strip()

    if input_text.lower() == "q":
        break
    if input_text:
        pre_prompt = pre_prompt + input_pre_prompt
    if len(input_text) > 50:
        input_text = input_text[:50]

    prompt = pre_prompt + input_text   

    message = client.messages.create(
        model=model,
        max_tokens=max_tokens,
        messages=[
            {
                "role": "user",
                "content": prompt,
            }
        ]
    )
    message = message.content[0].text.replace("\"", "")

    print(message, "\n")
