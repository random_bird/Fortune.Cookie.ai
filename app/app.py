import os
import logging
import anthropic
from pathlib import Path
from dotenv import load_dotenv
from flask import Flask, render_template, request

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s [%(threadName)s] [%(levelname)s] %(message)s"
)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

if Path(".env").exists():
    load_dotenv()

if "ANTHROPIC_API_KEY" not in os.environ:
    raise ValueError("Expected environment variable ANTHROPIC_API_KEY to exist.")

app = Flask(__name__)
anthropic_client = anthropic.Anthropic()
max_tokens = 50
model = os.environ.get("ANTHROPIC_MODEL", "claude-3-haiku-20240307")

pre_prompt = (
    "Write a fun or witty one-line fortune cookie message. "
    "Limit output to the message itself. "
    "Do not request additional info or text.\n"
)
input_pre_prompt = "Start the message with the following text:\n\n"


@app.route("/", methods=["POST", "GET"])
def generate_fortune():
    """Generate fortune cookie text to display on template html."""
    log_msg = ""
    if request.method == "POST":
        prefix = request.form["prefix"]
        if prefix.strip() != "":
            log_msg = f"[PREFIX] {prefix} "
    else:
        prefix = ""

    if len(prefix) > 50:
        prefix = prefix[:50]

    if prefix:
        prompt = pre_prompt + input_pre_prompt + prefix
    else:
        prompt = pre_prompt

    message = anthropic_client.messages.create(
        model=model,
        max_tokens=max_tokens,
        messages=[
            {
                "role": "user",
                "content": prompt,
            }
        ],
    )
    fortune_cookie = message.content[0].text.replace('"', "")

    log_msg += f"[FORTUNE] {fortune_cookie}"
    logging.info(log_msg)
    return render_template("index.html", fortune_cookie_text=fortune_cookie)


if __name__ == "__main__":
    from waitress import serve

    serve(app, host="0.0.0.0", port=5000)
