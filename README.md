# Fortune.Cookie.ai
This is a personal practice project to make a simple LLM web app serving AI generated fortune cookie messages.

![Screenshot of the web app](static/screenshot.png)

The images in this repo are made by me in [inkscape](https://gitlab.com/inkscape/inkscape) and are free to use non-commercially.

## Text generationr
### Prerequisite
Fortune.Cookie.ai uses Anthropic's Claude 3 Haiku model to generate fortune cookies.
To use it, you need to have an account with [Anthropic](https://anthropic.com) and obtain an
[API key](https://console.anthropic.com/settings/keys).

The API key needs to be available via the environment variable `ANTHROPIC_API_KEY`, e.g. via a `.env` file in the root folder as
`ANTHROPIC_API_KEY=<your_api_key>`.

### Local environment
If you do not use Docker, you have to set up your own Python environment. To create one, run:
```bash
python -m venv venv
```
Then activate the environment (depending on your shell) with
```bash
source venv/bin/activate
```
Install the dependencies into the environment:
```bash
pip install -r requirements.txt
```

### TUI
To start the TUI and generate fortune cookies start the TUI script with:
```bash
python fortune_cookie.py
```

### Web app
If you want to run the web app locally, then run:
```bash
python app/app.py
```

### Docker
A Docker image is available for use. If you have the API key in an `.env` file you can run the app with:
```bash
docker run -p 5000:5000 --env-file .env randombird/fortune_cookie:1.0.0
```

To start the web app via compose, add the API key as an environment variable to the compose file and run:
```bash
docker compose up
```
