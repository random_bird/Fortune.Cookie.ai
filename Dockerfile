FROM python:3.12

COPY app/ app/
COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir -r requirements.txt
RUN rm requirements.txt

WORKDIR /app

EXPOSE 5000/tcp

ENTRYPOINT ["python", "app.py"]
